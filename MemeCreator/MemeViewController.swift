//
//  MemeViewController.swift
//  MemeCreator
//
//  Created by Eddy Rogier on 13/09/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
//

import UIKit




class MemeViewController: UIViewController {
    
    
    
    @IBOutlet weak var mViewBottomL: UIView! // ViewResize
    @IBOutlet weak var bottomLabel: UILabel! // ici

    @IBOutlet weak var mContainerView: UIView!
    
    @IBOutlet weak var mViewContainerMeme: UIView!
    
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var mLabelTop: UILabel!
   
    
    
    
    
    var mStringPictureNames:String? // var containe name of the picture when user selected a picture in CollectionView
    var mCGRectPictureSize:CGRect?
    
    private var embeddedMContainerViewVC:UIViewController!
    
    //\/ -------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("ʕ•ᴥ•ʔ MEMEVIEWCONTROLLER")
        // Do any additional setup after loading the view, typically from a nib.
        setsRatioImage()
        print("memeViewContainerMeme", mViewContainerMeme.frame)
        print("bottom", mViewBottomL.frame)
        
        
        let toto = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 460))
        toto.backgroundColor = UIColor.red
        toto.alpha = 0.5
        self.view.addSubview(toto)
        
        let toto2 = UIView(frame: CGRect(x: 20, y: 420, width: 100, height: 40))
        toto2.backgroundColor = UIColor.blue
        toto2.alpha = 0.5
        self.view.addSubview(toto2)
        
        
    }

    // MARK: - Function for MEME
    /** ʕ•ᴥ•ʔ add an image and sets the ratio for matchs to the contain UIImageView.  */
    func setsRatioImage(){
        if mStringPictureNames != nil {
            mImageView?.image = UIImage(named: mStringPictureNames!)
            
            if (mImageView.image?.size.height)! <  (self.view.frame.size.height - self.mContainerView.frame.size.height)  {
                let newHeightRatio = calculatRatio(withOriginalHeight: (self.mImageView.image?.size.height)!,
                                                   originalWidth: (self.mImageView.image?.size.width)!,
                                                   deviceWidth: self.view.frame.size.width )
                mViewContainerMeme.frame.size.width = self.view.frame.size.width
                mViewContainerMeme.frame.size.height = newHeightRatio
                
                print("ʕ•ᴥ•ʔ taille de l'image : \(mViewContainerMeme.frame)")

                aligmentContainerImageView()
                
            } else {
                print("ʕ•ᴥ•ʔ non ratio")
                //mImageView.contentMode = UIViewContentMode.scaleAspectFit
            }
        }
    }
    
    /** Aligne l'image dans la vue primaire apres selection de celui ci. */
    func aligmentContainerImageView() {
        // align the container in the horizontal position
        let maxHeight               = self.view.frame.size.height - self.mContainerView.frame.size.height
        let posibilityHeight        = maxHeight - self.mViewContainerMeme.frame.size.height
        
        if mViewContainerMeme.frame.size.height < maxHeight {
            let deltaPosibilityHeight   = posibilityHeight / 2
            self.mViewContainerMeme.frame.origin.y = deltaPosibilityHeight
        } else {
            print("out of the limit !")
        }
        
    }
    
    /** calculates a new ratio for the imported images and recovers the height thanks to width what we know */
    func calculatRatio (withOriginalHeight originalHeight:CGFloat, originalWidth:CGFloat, deviceWidth:CGFloat) -> CGFloat{
        let newHeight = ( originalHeight / originalWidth ) * deviceWidth
        //        print("ʕ•ᴥ•ʔ new height : \(newHeight)")
        //        print("ʕ•ᴥ•ʔ originla height : \(originalHeight)")
        return newHeight
    }
    
    // MARK: - Function for sets LABEL
    
    
}

















