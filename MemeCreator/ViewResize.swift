//
//  ViewResize.swift
//  MemeCreator
//
//  Created by Eddy Rogier on 22/09/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
//

import Foundation
import UIKit


class ViewResize:UIView {
    
    var dotUpper : UIView!
    
    //\/ -------------------------
    override init(frame: CGRect) {
        super.init(frame:frame) // for using Custom... in code
        initialize()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder) // for using custom... in IB
        initialize()
    }
    func initialize(){
        print("ʕ•ᴥ•ʔ func initialise()")
        createDotUpper()
        prepareGesture()
        catchDefaultSize()
        
        
        
        
    }

    /** ʕ•ᴥ•ʔ create Dot for label. */
    // MARK: - Create Dot For View
    func createDotUpper(){
        print("ʕ•ᴥ•ʔ createDotUpper")
        let w:CGFloat = 10
        let h:CGFloat = 10
        let x:CGFloat = (self.frame.size.width / 2) - (w / 2)
        let y:CGFloat = 0
        
        let dotRect = CGRect(x: x, y: y, width: w, height: h)
        dotUpper = UIView(frame: dotRect)
        dotUpper.backgroundColor = UIColor.red
        self.addSubview(dotUpper)
    }
    
    
    // MARK: - Init Gesture
    func prepareGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(actionTap(gesture:)))
        dotUpper.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(actionPan(gesture:)))
        dotUpper.addGestureRecognizer(pan)
    }
    
    // MARK: - Mark Set Default size For ResizeView
    func catchDefaultSize(){
        
        //print("ʕ•ᴥ•ʔ catchDefaultSiz", self.superview?.frame as Any)
        
        
        viewResizeDefaultOrigin = self.frame.origin.y
        viewResizeDefaultSize   = self.frame.size.height
        print("----------------->",viewResizeDefaultOrigin, viewResizeDefaultSize)
    }
    
    //\/ -------------------------
    //var actionTapLocationY:CGFloat!

    // MARK: - ActionTap
    func actionTap(gesture:UITapGestureRecognizer){
        print("ʕ•ᴥ•ʔ tap")

      
    }
    
    // MARK: - properties For resizeview
    var beginL:CGFloat!
    var beginY:CGFloat!
    var beginH:CGFloat!
    var changedLselfY:CGFloat!
    
    var isMinimumSize:Bool = false
    var viewResizeMinimumSize:CGFloat = 40
    
    var viewResizeDefaultSize:CGFloat!
    var viewResizeDefaultOrigin:CGFloat!
    
    // MARK: - Gesture Began/Changed/Default/Ended etc
    func actionPan(gesture:UIPanGestureRecognizer){
        //print("pan")
        
        switch gesture.state {
                    case .began     :
                        self.backgroundColor = UIColor.red
                        beginL = gesture.location(in: self).y
                        beginY = self.frame.origin.y
                        beginH = self.frame.size.height
                        //print(beginL, beginY, beginH)
                 
                        break;
            
                    case .changed   :
                        self.backgroundColor = UIColor.blue
                        changedLselfY = gesture.location(in: self.dotUpper).y

                        // direction ʕ•ᴥ•ʔ
                        if changedLselfY < 0 {
                            print("ʕ•ᴥ•ʔ HAUT", self.frame.origin.y, self.frame.size.height)
                            //\/ -------------------------
                            positionAdjust(position: ViewResize.direction.top)
                        } else {
                            print("BAS  ʕ•ᴥ•ʔ", self.frame.origin.y, self.frame.size.height)
                            //\/ -------------------------
                            positionAdjust(position: ViewResize.direction.bottom)
                        }
            
            
                    case .ended     :
                        print("gesture.state = ended")
                        self.backgroundColor = UIColor.green
                        
                        if isMinimumSize {
                            print("gesture.state = ended IN IF")
                            self.frame.origin.y     = viewResizeDefaultOrigin
                            self.frame.size.height  = viewResizeDefaultSize
                            isMinimumSize = false
                        }
                    case .cancelled :
                        print("gesture.state = cancelled")
                    default:
                        print("gesture.state = default")
                        self.backgroundColor = UIColor.white
                        if isMinimumSize {
                            print("gesture.state = ended IN DEFAULT")
                            self.frame.origin.y     = viewResizeDefaultOrigin
                            self.frame.size.height  = viewResizeDefaultSize
                            isMinimumSize = false
                        }
                        break;
                    }

    }
    
    // MARK: - Enum Direction with Function PositionAdjust
    enum direction {
        case top
        case bottom
    }
    
    
    func positionAdjust(position:direction){
        switch position {
        
        case .top:
            if self.frame.size.height >= viewResizeDefaultSize || self.frame.origin.y <= viewResizeDefaultOrigin {
                self.frame.origin.y    += changedLselfY // location y constante
                self.frame.size.height -= changedLselfY
            } else {
                print("========================================STOP from top")
                // FIXME: "FIXME" : (っ-●益●)っ ~~ viewResize depasse les limites
                isMinimumSize           = true
            }
            
            
        case .bottom:
            //bas
            if self.frame.size.height >= viewResizeDefaultSize || self.frame.origin.y <= viewResizeDefaultOrigin {
                self.frame.origin.y    += changedLselfY
                self.frame.size.height += -changedLselfY
            } else {
                print("========================================STOP from bottom |")
                
                // FIXME: "FIXME" : (っ-●益●)っ ~~ viewResize depasse les limites
                isMinimumSize           = true
            }
            
        }
    }

    
    
    
    
    
    
    
    
    
    
    
 
}
