//
//  CollectionViewController.swift
//  MemeCreator
//
//  Created by Eddy Rogier on 14/09/2017.
//  Copyright © 2017 EddyRogier. All rights reserved.
// MyCollectionViewCell

import UIKit



class CollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // Section : CollectionView 01 ʕ•ᴥ•ʔ
    @IBOutlet weak var mCollectionView: UICollectionView!
    var arrayPictures = ["i1","i2","i3","i4","i5","i6","i7","i8","i9","test","1326x986","1000x1000","i10","i11","i12","i13","i14","i15"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //\/ -------------------------
        
        // Section CollectionView 01 ʕ•ᴥ•ʔ
        self.mCollectionView.delegate = self
        self.mCollectionView.dataSource = self
        //\/ -------------------------
        
        // Section : Gesture 02 ʕ•ᴥ•ʔ
        
    }

    
    // MARK: - CollectionView
    // resize cell ʕ•ᴥ•ʔ
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // for spacing sets "min-spacing"
            //let cellNumberPerRow    =
        let width               = (self.view.frame.size.width ) / (3) //some width
        let height              = width * 1 //ratio
        
        return CGSize(width: width, height: height);
        
//        let width = (self.view.frame.size.width - 15 * 4) / 4 //some width
//        let height = width * 1 //ratio
//        return CGSize(width: width, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayPictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCollectionViewCell", for: indexPath) as! MyCollectionViewCell
        cell.mImageCollectionViewcell.image = UIImage(named: arrayPictures[indexPath.row])
        //cell.backgroundColor = UIColor.yellow
        return cell
    }
    
    // Section : SendDataAfterClick 02 ʕ•ᴥ•ʔ
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("selected")
//        print(indexPath.row)
        // manipulation de la segue et envoie le sender dans la fonction prepare segue
        performSegue(withIdentifier: "backMemeViewController", sender: arrayPictures[indexPath.row])
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // execute the code if the destination segue is really of kind MemeViewController ʕ•ᴥ•ʔ
        // depending of the type data u can use the delegation . ʕ•ᴥ•ʔ
        // fil d'Ariane avec MemeViewController ʕ•ᴥ•ʔ
        let mMemeViewController = segue.destination as! MemeViewController
        //print("sender result",sender as! String)
        // get sender and force cast beacause it s will be always a string.
        let stringPictureName:String = sender as! String
        // sets the string variable from MemeViewController to here.
        mMemeViewController.mStringPictureNames = stringPictureName
        //mMemeViewController.mCGRectPictureSize
        print("sender", sender as Any)

    }
    
    
    
    
    
    
    
    

}
